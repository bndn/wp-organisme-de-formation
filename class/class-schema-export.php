<?php
/*
 * class-schema-export.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class SchemaExport
{
    public $id;
    public $nom;
    public $cols = array();
    
    public function __construct($schema_id = "all")
    {
        if ($schema_id == "all")
            $this->init_all_cols();
        elseif ($schema_id)
        {
            $s = get_user_meta(get_current_user_id(), "schema_export", true);
            if (is_object($s[$schema_id]))
            {
                $this->id = $s[$schema_id]->id;
                $this->nom = $s[$schema_id]->nom;
                $this->cols = $s[$schema_id]->cols;
            }
        }
    }
    
    public function save()
    {
        $schema_export = get_user_meta(get_current_user_id(), "schema_export", true);
        if (!is_array($schema_export))
            $schema_export = array();
        $schema_export[$this->id] = $this;
        return update_user_meta(get_current_user_id(), "schema_export", $schema_export);
    }
    
    public function init_all_cols()
    {
        /*
        global $wpdb;
        
        $query = "SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'session';";
        $sessions = array_map(function($a) { return $a[0]; }, $wpdb->get_results($query, ARRAY_N));
        
        $query = "SELECT DISTINCT meta_key FROM {$wpdb->prefix}postmeta WHERE post_id IN (".join(",", $sessions).");";
        $cols = $wpdb->get_results($query, ARRAY_N);
        foreach($cols as $c)
            $this->cols[] = (object) array("db_text" => $c[0], "new_text" => $c[0], "entite" => "session");
        
        $query = "SELECT DISTINCT meta_key FROM {$wpdb->prefix}wpof_client;";
        $cols = $wpdb->get_results($query, ARRAY_N);
        foreach($cols as $c)
            $this->cols[] = (object) array("db_text" => $c[0], "new_text" => $c[0], "entite" => "client");
            
        $query = "SELECT DISTINCT meta_key FROM {$wpdb->prefix}wpof_session_stagiaire;";
        $cols = $wpdb->get_results($query, ARRAY_N);
        foreach($cols as $c)
            $this->cols[] = (object) array("db_text" => $c[0], "new_text" => $c[0], "entite" => "stagiaire");
        */
        foreach(array_keys(get_class_vars("SessionFormation")) as $k)
            $this->cols[] = (object) array("db_text" => $k, "new_text" => $k, "entite" => "session");
        
        foreach(array_keys(get_class_vars("Client")) as $k)
            $this->cols[] = (object) array("db_text" => $k, "new_text" => $k, "entite" => "client");
        
        foreach(array_keys(get_class_vars("SessionStagiaire")) as $k)
            $this->cols[] = (object) array("db_text" => $k, "new_text" => $k, "entite" => "stagiaire");
        
        $this->nom = __("Défaut");
        $this->id = "all";
    }
    
    public function get_choix_cols()
    {
        ob_start();
        
        if (is_array($this->cols)) : ?>
        <ul class="opaga opaga2 colonnes_export export">
        <li class="li-tab head li-head visible">
        <div class="center thin"></div>
        <div class="center thin"><input type="checkbox" name="all" checked="checked" data-list="cols-sortable" /></div>
        <div><?php _e("Entité OPAGA"); ?></div>
        <div><?php _e("Colonne dans la base de données"); ?></div>
        <div><?php _e("Nom pour l'export"); ?></div>
        <div><?php _e("Valeur par défaut"); ?></div>
        </li>
        </ul>
        
        <?php $this->the_choix_cols_list(); ?>
        <?php endif;
        
        return ob_get_clean();
    }
    
    public function get_custom_col()
    {
        return (object) array("db_text" => "", "new_text" => "", "entite" => "perso");
    }
    
    // Affichage de la liste des colonnes à choisir
    public function the_choix_cols_list()
    {
        ?>
        <ul class="opaga opaga2 colonnes_export export" id="cols-sortable">
        
        <?php
        foreach($this->cols as $k => $c)
        {
            $c = (object) $c;
            $this->the_choix_cols_row($k, $c);
        }
        ?>
        
        </ul>
        <?php
    }
    
    // Affichage d'une colonne (sous forme de ligne, row)
    public function the_choix_cols_row($k, $c)
    {
        ?>
        <li class="li-tab li-sort colonne global_check visible <?php echo $c->entite; ?>">
        <div class="center thin"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span></div>
        <div class="center thin"><input type="checkbox" name="<?php echo $k; ?>" checked="checked" /></div>
        <div class="entite"><?php echo $c->entite; ?></div>
        <div class="db_text"><?php echo $c->db_text; ?></div>
        <div class="new_text"><input type="text" name="<?php echo $c->db_text; ?>" value="<?php echo $c->new_text; ?>" /></div>
        <div class="valeur"><?php if ($c->entite == "perso") : ?><input type="text" name="<?php echo $c->db_text; ?>_value" value="<?php echo $c->valeur; ?>" /><?php endif; ?></div>
        </li>
        <?php
    }
}

add_action('wp_ajax_add_colonne_export', 'add_colonne_export');
function add_colonne_export()
{
    $schema = new SchemaExport(null);
    $new_col = $schema->get_custom_col();
    $schema->the_choix_cols_row("perso".time(), $new_col);
    die();
}

add_action('wp_ajax_enregistrer_schema', 'enregistrer_schema');
function enregistrer_schema()
{
    $reponse = array();
    
    $schema = new SchemaExport(null);
    
    $schema->nom = $_POST['nom_schema'];
    $schema->id = sanitize_title($_POST['nom_schema']);
    $schema->cols = $_POST['cols'];
    $reponse['log'] = $schema->save();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_supprimer_schema', 'supprimer_schema');
function supprimer_schema()
{
    $reponse = array();
    
    $schema_export = get_user_meta(get_current_user_id(), "schema_export", true);
    if (is_array($schema_export) && in_array($_POST['schema'], array_keys($schema_export)))
    {
        unset($schema_export[$_POST['schema']]);
        update_user_meta(get_current_user_id(), "schema_export", $schema_export);
        $reponse['log'] = true;
    }
    else
        $reponse['log'] = false;
        
    echo json_encode($reponse);
    die();
}
