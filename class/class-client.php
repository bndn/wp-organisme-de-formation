<?php
/*
 * class-client.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

class Client
{
    public $adresse = "";
    public $code_postal = "";
    public $ville = "";
    public $telephone = "";
    public $nom = "";
    public $contact_id = -1;
    public $contact = "";
    public $contact_email = "";
    public $num_of = "";
    public $nature_formation = "";
    public $financement = "";
    public $financement_complement = "";
    public $opco = "";
    public $no_dossier_opco = "";
    public $etat_session = "initial";
    
    public $numero_contrat ="";
    
    public $tarif_heure = 0;
    public $tarif_total_chiffre = 0;
    public $tarif_total_autres_chiffre = 0;
    public $autres_frais = "";
    public $exe_comptable = array();
    
    public $nb_heure;
    public $nb_heure_decimal = 0;
    public $nb_heure_estime;
    public $nb_heure_estime_decimal = 0;
    public $creneaux = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    
    public $stagiaires = array();
    public $nb_stagiaires = 0;
    public $nb_confirmes = 0;
    public $nb_heures_stagiaires = 0;
    public $session_formation_id = -1;
    public $id = -1;
    
    // token d'accès aux infos
    public $token = "";
    public $token_time = 0;
    public $acces = 0;
    
    // table suffix
    private $table_suffix;
    
    public function __construct($session_id, $client_id = -1)
    {
        global $suffix_client;
        global $wpof;
        $this->table_suffix = $suffix_client;
        
        $this->session_formation_id = $session_id;
        $session_formation = get_session_by_id($session_id);
        
        if ($client_id > 0)
        {
            $this->id = $client_id;
            
            $meta = $this->get_meta();
            
            foreach($meta as $m)
            {
                if (isset($this->{$m['meta_key']}))
                {
                    if (is_array($this->{$m['meta_key']}))
                        $this->{$m['meta_key']} = unserialize(stripslashes($m['meta_value']));
                    else
                        $this->{$m['meta_key']} = stripslashes($m['meta_value']);
                }
            }
            
            /*$this->creneaux = array();
            
            // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
            $creneaux_from_db = json_decode($this->get_meta("creneaux"), true);
            */
            // on définit tous les créneaux comme actifs par défaut ou si $creneaux_from_db[id] n'est pas définit
            
            if (empty($this->creneaux) && is_array($session_formation->creneaux))
                foreach($session_formation->creneaux as $d)
                    foreach($d as $c)
                        $this->creneaux[$c->id] = 1;
            else
                foreach($session_formation->creneaux as $d)
                    foreach($d as $c)
                        if (!isset($this->creneaux[$c->id]))
                            $this->creneaux[$c->id] = 0;

            $this->calcule_temps_session();
            
            $this->calcule_tarif();
            
            $doc_contexte = 0;
            
            // cas de la sous-traitance : le client est un autre OPAC
            if ($this->financement == "opac")
            {
                $doc_contexte = $wpof->doc_context->sous_traitance | $wpof->doc_context->client;
                if ($this->nb_heures_stagiaires == 0)
                    $this->nb_heures_stagiaires = $this->nb_stagiaires * $this->nb_heure_estime_decimal;
            }
            else
            {
                $doc_contexte = $wpof->doc_context->direct | $wpof->doc_context->client;
                
                // si convention en direct, on rectifie éventuellement le nombre de stagiaires inscrit dans la BD
                if ($this->nb_stagiaires != count($this->stagiaires))
                    $this->update_meta("nb_stagiaires", count($this->stagiaires));
                foreach ($this->stagiaires as $i)
                {
                    if (get_stagiaire_meta($i, "confirme") != 0)
                        $this->nb_confirmes++;
                }
            }

            // documents nécessaires
            foreach($wpof->documents->term as $doc_type => $doc)
            {
                if (($doc->contexte & $doc_contexte) == $doc_contexte)
                    $this->doc_necessaire[] = $doc_type;
            }
            $this->doc_suffix = "c".$this->id;
        }
    }
    
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session formation
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->client, $this->id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
        }
    }
    
    public function init_stagiaires()
    {
        global $SessionStagiaire;
        
        foreach($this->stagiaires as $stagiaire_id)
        {
            $SessionStagiaire[$stagiaire_id] = new SessionStagiaire($this->session_formation_id, $stagiaire_id);
            if ($SessionStagiaire[$stagiaire_id] != 0)
                $this->nb_confirmes++;
        }
        
    }
    
    public function get_nom()
    {
        global $wpof;
        $nom = "";
        if (champ_additionnel('numero_contrat'))
            $nom = "<strong>".$this->numero_contrat."</strong> ";
        $nom .= (empty($this->nom)) ? __("Sans nom")." ID = ".$this->id : $this->nom;
        
        return $nom;
    }
    
    public function get_nb_heure_stagiaire()
    {
        if ($this->financement != "opac")
        {
            $total = 0;
            
            foreach($this->stagiaires as $user_id)
            {
                $stagiaire = new SessionStagiaire($this->session_formation_id, $user_id);
                $total += $stagiaire->nb_heure_estime_decimal;
            }
            return $total;
        }
        else 
            return $this->nb_heures_stagiaires;
    }
    
    public function get_meta($meta_key = null)
    {
        return get_client_meta($this->id, $meta_key);
    }

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);
        
        // si le client n'est pas encore entré dans la base on lui crée un nouvel id
        if ($this->id < 1)
            $this->id = $this->last_client_id() + 1;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, client_id, meta_key, meta_value)
            VALUES ('%d', '%d', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->id, $meta_key, $meta_value, $meta_value);
        
        return $wpdb->query($query);
    }
    
    // Supprime un élément d'un tableau de sous-entité (stagiaires)
    public function supprime_sous_entite($tab_name, $id)
    {
        if (isset($this->$tab_name) && is_array($this->$tab_name))
        {
            $key = array_search($id, $this->$tab_name);
            unset($this->$tab_name[$key]);
            $this->update_meta($tab_name, $this->$tab_name);
        }
    }

    // Calcule le tarif total
    public function calcule_tarif()
    {
        if ($this->nb_heure_decimal > 0)
            $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
            
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
        $this->tarif_total_autres_lettre = num_to_letter($this->tarif_total_autres_chiffre);
    }
    

    // Calcule le temps (en heures) de la session contractualisé avec ce client (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        foreach($session_formation->creneaux as $date => $tab_date)
            foreach($tab_date as $creno)
            {
                if ($this->creneaux[$creno->id] != 0)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                    $this->dates_array[] = $date;
                }
            }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if ($this->nb_heure_estime_decimal == 0)
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }
    
    
    public function delete()
    {
        // supression du client du tableau clients de la session
        $session = get_session_by_id($this->session_formation_id);
        
        foreach($this->stagiaires as $user_id)
        {
            $stagiaire = get_stagiaire_by_id($this->session_formation_id, $user_id);
            $stagiaire->delete();
            // suppression du stagiaire dans le tableau inscrits de la session TODO : supprimer ce tableau définitivement
            $session->supprime_sous_entite("inscrits", $user_id);
        }
        
        $session->supprime_sous_entite("clients", $this->id);
        
        // suppression du client dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE client_id = '%d';",
            $this->id
        );
        
        return $wpdb->query($query);
    }
    
    private function last_client_id()
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = "SELECT MAX(`client_id`) FROM $table;";
        return $wpdb->get_var($query);
    }
    
    public function get_the_board()
    {
        global $SessionFormation;
        global $SessionStagiaire;
        global $wpof;
        
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        $session = get_session_by_id($this->session_formation_id);
        $this->init_docs();
        
        ob_start();
        
        ?>
        <div id="tab-c<?php echo $this->id; ?>" class="board board-client edit-data client-<?php echo $this->id; ?>">
            <div class="infos-client flexrow">
            <?php $this->the_infos_client(); ?>
            </div>
            
            <fieldset>
                <legend><?php _e("Documents administratifs pour la session"); ?></legend>
                <?php echo get_gestion_docs($this); ?>
            </fieldset>
                
            <fieldset><legend><?php _e("Créneaux contractualisés"); ?></legend>
            <?php echo get_icone_aide("client_creneaux", __("Créneaux contractualisés")); ?>
            <?php
                if (count($session->creneaux) == 0)
                    echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                else
                    echo $session->get_html_creneaux(false, $this);
            ?>
            </fieldset>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function the_board()
    {
        echo $this->get_the_board();
    }
    
    public function the_infos_client()
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        
        ?>
        <div class="coordonnees">
        <?php
            if (champ_additionnel('numero_contrat'))
                echo get_input_jpost($this, "numero_contrat", array('input' => 'text', 'label' => __("Numéro de contrat")));
            
            echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom"), 'postprocess' => 'update_client_nom'));
            echo get_input_jpost($this, "adresse", array('textarea' => '', 'label' => __("Adresse")));
            echo get_input_jpost($this, "code_postal", array('input' => 'text', 'label' => __("Code postal")));
            echo get_input_jpost($this, "ville", array('input' => 'text', 'label' => __("Ville")));
            echo get_input_jpost($this, "telephone", array('input' => 'text', 'label' => __("Téléphone")));
            echo get_input_jpost($this, "contact", array('input' => 'text', 'label' => __("Contact")));
            echo get_input_jpost($this, "contact_email", array('input' => 'text', 'label' => __("E-mail contact")));
            if ($this->financement == 'opac')
                echo get_input_jpost($this, "num_of", array('input' => 'text', 'label' => __("Numéro de déclaration d'activité de formation")));
        ?>
        </div>
        
        <div class="bpf">
        <?php
            $financement_args = array('select' => '', 'label' => __("Principale source de financement"), 'postprocess' => 'toggle_opco');
            if ($this->financement == "opac") $financement_args["readonly"] = 1;
            echo get_input_jpost($this, "financement", $financement_args);
            
            $opco_jpost_args = array('select' => '', 'label' => __("Opérateur de compétences"), 'first' => 'Autre', 'class' => 'toggle-opco');
            $no_dossier_opco_jpost_args = array('input' => 'text', 'label' => __("Numéro de dossier chez l'OPCO"), 'class' => 'toggle-opco');
            if (!in_array($this->financement, array("mutu8", "mutu7")))
            {
                $opco_jpost_args['display'] = 'none';
                $no_dossier_opco_jpost_args['display'] = 'none';
            }
            echo get_input_jpost($this, "opco", $opco_jpost_args);
            echo get_input_jpost($this, "no_dossier_opco", $no_dossier_opco_jpost_args);
                
            if ($this->financement != 'opac')
                echo get_input_jpost($this, "nature_formation", array('select' => '', 'label' => __("Objectif de la prestation")));
            
            $taxe = ($wpof->of_hastva) ? __("HT") : __("TTC");
            echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Tarif total en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client+update_pour_infos_session'));
            echo get_input_jpost($this, "tarif_total_autres_chiffre", array('input' => 'number', 'step' => '0.01', 'label' => __("Dont autres frais non pédagogiques en ").$wpof->monnaie." ".$taxe, 'postprocess' => 'update_pour_infos_client'));
            echo get_input_jpost($this, "autres_frais", array('input' => 'text', 'label' => __("Nature des autres frais")));
            
            if ($this->financement == "opac")
            {
                echo get_input_jpost($this, "nb_stagiaires", array('input' => 'number', 'step' => 1, 'min' => 0, 'label' => 'Nombre de stagiaires'));
                echo get_input_jpost($this, "nb_heures_stagiaires", array('input' => 'number', 'step' => 1, 'min' => 0, 'label' => "Nombre d'heures × stagiaires"));
            }
            echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'step' => 0.25, 'min' => 0, 'label' => "Nombre d'heures estimé"));
            
            ?>
        </div>
        <div class="pour-infos">
            <?php echo $this->get_pour_infos_box(); ?>
            <div class="icones">
                <?php if ($this->financement != "opac") : ?>
                <p class="icone-bouton dynamic-dialog" data-function="new_stagiaire" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("Ajouter stagiaire"); ?></p>
                <?php echo get_icone_aide("inscrire_stagiaires", __("Stagiaires")); ?>
                <?php else: ?>
                <?php echo get_icone_aide("sous_traitance_stagiaires", __("Stagiaires")); ?>
                <?php endif; ?>
                
                <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-parent=".client-<?php echo $this->id; ?>">
                <span class="dashicons dashicons-dismiss" > </span>
                <?php _e("Supprimer ce client"); ?>
                </p>
                
                <p class="token-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>" data-parent=".client-<?php echo $this->id; ?>">
                <span class="dashicons dashicons-admin-network" > </span>
                <?php _e("Envoyer accès privé"); ?>
                </p>
                
                <p class="token" id="token<?php echo $this->id; ?>">
                <?php echo $this->token; ?>
                </p>
                
                <?php if ($role == "admin") : ?>
                <p>ID : <?php echo $this->id; ?></p>
                <p class="icone-bouton dynamic-dialog" data-function="sql_session_formation" data-clientid="<?php echo $this->id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("SQL client"); ?></p>
                <?php endif; ?>
            </div>
        </div>
        
        
        <?php
    }
    
    public function get_pour_infos_box()
    {
        global $wpof;
        ob_start();
        ?>
            <h3><?php _e("Pour information"); ?></h3>
            <p><?php _e("Dates concernées"); ?> : <span class="dates_concernees"><?php echo (empty($this->dates_texte)) ? __("aucune, cochez des créneaux pour choisir les dates") : $this->dates_texte; ?></span></p> 
            <p><?php _e("Tarif horaire"); ?> : <span class="tarif_heure"><?php echo $this->tarif_heure." ".$wpof->monnaie_symbole; ?></span> </p>
            <p><?php _e("Nombre d'heures"); ?> : <span class="nb_heure"><?php echo $this->nb_heure; ?></span> </p>
            <p><?php _e("Tarif total (lettres)"); ?> : <span class="tarif_total_lettre"><?php echo $this->tarif_total_lettre; ?></span> </p>
            <?php if ($this->tarif_total_autres_chiffre > 0) : ?>
            <p><?php _e("Dont autres frais"); ?> : <span class="tarif_total_autres_lettre"><?php echo $this->tarif_total_autres_lettre; ?></span> </p>
            <?php endif; ?>
        <?php
        return ob_get_clean();
    }
    
    /*
     * Interface avec le client
     */
    public function the_interface()
    {
        echo $this->get_the_interface();
    }
    
    public function get_the_interface()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        
        echo hidden_input("default_main_tab", (isset($_SESSION['main-tabs'])) ? $_SESSION['main-tabs'] : 0);
        ?>
        <p><strong><?php echo $this->nom; ?></strong> <?php if (!empty($this->contact)) echo __("représenté par")." ".$this->contact; ?></p>
        <h2><?php echo $session_formation->titre_formation; ?></h2>
        <p><strong><?php _e("Dates"); ?></strong> <?php echo $session_formation->dates_texte; ?></p>
        <div id="main-tabs">
            <ul>
                <li><a href="#tab-presentation"><?php _e("La formation"); ?></a></li>
                <li><a href="#tab-planning"><?php _e("Planning"); ?></a></li>
                <li><a href="#tab-documents"><?php _e("Documents"); ?></a></li>
            </ul>
            
            <div id="tab-presentation" class="client" data-id="<?php echo $this->id; ?>">
            <?php echo $session_formation->get_html_presentation(array('entite' => $this)); ?>
            </div> <!-- presentation -->
            
            <div id="tab-planning" class="client" data-id="<?php echo $this->id; ?>">
            <?php echo $session_formation->get_decoupage_temporel($this->creneaux); ?>
            </div> <!-- planning -->
            
            <div id="tab-documents">
                <?php
                global $Documents;
                $this->init_docs();
                $liste_doc = "";
                foreach ($this->doc_uid as $docuid)
                {
                    $doc = $Documents[$docuid];
                    if ($doc->visible)
                        $liste_doc .= "<li class='doc {$doc->type}'>{$doc->link_name}</li>";
                }
                if ($liste_doc != "") : ?>
                <ul class="liste-doc-stagiaire">
                <?php echo $liste_doc; ?>
                </ul>
                <?php else : ?>
                <p><?php echo $wpof->doc_pas_de_docs; ?></p>
                <?php endif; ?>
            </div> <!-- documents -->
        
        </div>
        
        <?php
        return ob_get_clean();
    }
    
}

function get_client_meta($client_id, $meta_key)
{
    global $wpdb;
    global $suffix_client;
    $table_client = $wpdb->prefix.$suffix_client;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_client
            WHERE client_id = '%d'
            AND meta_key = '%s';",
            $client_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_client WHERE client_id = '%d';", $client_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

function get_client_by_id($session_id, $client_id)
{
    global $Client;
    
    $client_id = (integer) $client_id;
    
    if (!isset($Client[$client_id]))
        $Client[$client_id] = new Client($session_id, $client_id);
        
    return $Client[$client_id];
}
