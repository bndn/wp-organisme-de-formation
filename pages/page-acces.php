<?php
/*
 * page-acces.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-token.php");

add_filter( 'the_posts', 'generate_acces_page', -10 );
function generate_acces_page($posts)
{
    global $wp, $wp_query, $wpof;

    $url_slug = $wpof->url_acces; // slug de la page d'acces

    if (!defined('ACCES_PAGE') && (strtolower($wp->request) == $url_slug))
    {
        define( 'ACCES_PAGE', true );

        // create a fake virtual page
        $post = new stdClass;
        $post->post_author    = 1;
        $post->post_name      = $url_slug;
        $post->guid           = home_url() . '/' . $url_slug;
        $post->post_title     = $wpof->title_acces;
        $post->post_content   = get_acces_content();
        $post->ID             = -35;
        $post->post_type      = 'page';
        $post->post_status    = 'static';
        $post->comment_status = 'closed';
        $post->ping_status    = 'closed';
        $post->comment_count  = 0;
        $post->post_date      = current_time( 'mysql' );
        $post->post_date_gmt  = current_time( 'mysql', 1 );
        $posts                = NULL;
        $posts[]              = $post;

        // make wpQuery believe this is a real page too
        $wp_query->is_page             = true;
        $wp_query->is_singular         = true;
        $wp_query->is_home             = false;
        $wp_query->is_archive          = false;
        $wp_query->is_category         = false;
        unset( $wp_query->query[ 'error' ] );
        $wp_query->query_vars[ 'error' ] = '';
        $wp_query->is_404 = false;
    }

    return $posts;
}

function get_acces_content()
{
    global $wpof;
    $html = "";
    
    $acces_interdit = __("Vous n'avez pas accès à cette ressource");
    if (!isset($_GET['t']))
        return $acces_interdit;
    
    $token = new Token($_GET['t']);
    $data = $token->get_acces();
    
    if (isset($data['erreur']))
    {
        $html .= "<p class='erreur'>".$data['erreur']."</p>";
        if (isset($data['session_id']))
            $html .= get_reset_token_form($data['session_id']);
        return $html;
    }
    
    $session = get_session_by_id($data['session_id']);
    $session->init_clients();
    $session->init_stagiaires();
    
    $user = $data['user'];
    
    $html .= $user->get_the_interface();
    
    return $html;
}

function get_reset_token_form($session_id)
{
    ob_start();
    
    ?>
    <div id="risette">
    <p><?php _e("Saisissez votre adresse email pour recevoir un nouveau lien"); ?></p>
    <input name="email" type="email" />
    <input type="hidden" name="session_id" value="<?php echo $session_id; ?>" />
    <div class="icone-bouton token-reset"><?php _e("Envoyer"); ?></div>
    <p class="message"></p>
    </div>
    <?php
    
    return ob_get_clean();
}
