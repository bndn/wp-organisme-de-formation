jQuery(document).ready(function($)
{
    $(".token-entity").click(function(e)
    {
        e.preventDefault();
        
        session_id = $(".id.session").attr('data-id');
        object_class = $(this).attr('data-objectclass');
        id = $(this).attr('data-id');
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'new_token',
                'session_id': session_id,
                'object_class': object_class,
                'id': id,
            },
            function(response)
            {
                param = JSON.parse(response);
                if (param.erreur === undefined)
                    $('#token' + id).html("<p class='succes'>" + param.succes + "</p>");
                else
                    $('#token' + id).html("<p class='erreur'>" + param.erreur + "</p>");
            }
        );
    });

    $(".token-reset").click(function(e)
    {
        e.preventDefault();
        
        parent = $(this).parent();
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'reset_token_with_email',
                'email': parent.find("input[name='email']").val(),
                'session_id': parent.find("input[name='session_id']").val(),
            },
            function(response)
            {
                param = JSON.parse(response);
                if (param.erreur === undefined)
                    parent.children(".message").html("<p class='succes'>" + param.succes + "</p>");
                else
                    parent.children(".message").html("<p class='erreur'>" + param.erreur + "</p>");
            }
        );
    });
});
